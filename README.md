## Using

To run the docker containers just run the command `docker-compose up -d`
which will start the containers in detached mode.

Upon creation a network called data-network will be created which can be
used in other docker compose files or via docker.

This can be accomplished by specifying the `--network` argument for `docker run`

```
docker run --network data-network
```

Other _docker-compose.yml_ to which makes use of the
`data-network`

_docker-compose.yml_

```yml

//. My other containers and configs here

networks:
  default:
    external:
      name: data-network
```


## Configuration

The only configuration currently needed is to set up an sa user password
for the mssql container which can be done by creating a .env file in the
root of the project.

_.env_
```
SA_PASSWORD={my_sql_password}
```